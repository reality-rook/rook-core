from bottle import run, get, post, request, abort
import mysql.connector

import os
import string
import json

db_config = {}
with open('config/database.conf', 'r') as conf_file:
    config = json.loads(conf_file.read())

potential_module_paths = []
for file_name in os.listdir('modules'):
    potential_module_paths.append('modules/' + file_name)
for file_name in os.listdir('core_modules'):
    potential_module_paths.append('core_modules/' + file_name)

modules = {} # Keys are module names, also known as the folder names.
# Each module is comprised of a top path to the module folder,
# a list containing tables,
# a list containing requirements,
# and a list containing routes.
# It should look like:
# modules['example_module']{
#     'path': 'modules/example_module',
#     'tables': [],
#     'requires': [],
#     'routes': []
# }

# For table name, requirement name, and route name checking. No special characters or spaces should be used in these.
name_character_whitelist = set(string.ascii_letters + '-_')
for potential_module_path in potential_module_paths:
    # To check if a module exists it needs to fit the structure of
    # module_folder
    #  - python folder for scripts
    #  - structure folder to prepare it
    #     - require file for connections to other modules
    #     - route file to add routes into the webserver
    #     - tables file to verify and/or create/update the database tables.
    if not os.path.isdir(potential_module_path):
        continue

    files_in_dir = os.listdir(potential_module_path)
    if not 'python' in files_in_dir or not 'structure' in files_in_dir:
        print('Module directory at ' + potential_module_path + ' does not contain the required sub directories.')
        continue

    if not os.path.isdir(potential_module_path + '/python') or not os.path.isdir(potential_module_path + '/structure'):
        print('Module directory at ' + potential_module_path + ' contains python and structure files, which must be directories.')
        continue

    structure = os.listdir(potential_module_path + '/structure')
    if not 'require' in structure or not 'routes' in structure or not 'tables' in structure:
        print('Structure directory at ' + potential_module_path + ' is missing structure files.')
        continue

    if (
        os.path.isdir(potential_module_path + '/structure/require') or
        os.path.isdir(potential_module_path + '/structure/routes') or
        os.path.isdir(potential_module_path + 'structure/tables')
        ):
        print('Structure directory at ' + potential_module_path + ' has a structure directy with sub directories instead of files.')
        continue

    # Files all exist and can be accessed to make sure their formats provide the data required to function.
    module_path = potential_module_path
    print('Potential module found: ' + module_path)

    module_require = []
    require_failed_format = False
    with open(module_path + '/structure/require', 'r') as require_file:
        for line in require_file.readlines():
            if require_failed_format: # If the format failed, skip the next iteration because the file wont be used anyway.
                continue
            if len(line.split(' ')) != 3 and (line != '' or line != '\n'):
                print('Module found at ' + module_path + ' has an improperly formatted require file and will not be loaded. ("' + line + '" does not have 3 parameters)')
                require_failed_format = True
                continue
            if line.split(' ')[1] != '>':
                print ('Module found at ' + module_path + ' has an improperly formatted line. ("' + line + '" is missing ">" statement).')
                require_failed_format = True
                continue
            require.append((line.split(' ')[0], line.split(' ')[2]))

    if require_failed_format:
        continue
    # Verify routes and tables now, requirements are going to have to come *after* all correctly formatted modules are found.
    # Requirements for incorrectly formatted or error prone modules will be dropped and modules that depend on them
    # will be removed from the dict of modules to load, with a warning in the terminal.
    module_routes = []
    with open(module_path + '/structure/routes', 'r') as routes_file:
        for line in routes_file.readlines():
            if len(line.split(' ')) != 5  and (line != '' or line != '\n'):
                print('"' + line + '" cannot be loaded as route: it does not have 5 parameters.')
                continue
            if line.split(' ')[1] != '>' or line.split(' ')[3] != '>':
                print('"' + line + '" cannot be loaded as route: it does not have required ">" statements.')
                continue
            if not os.path.isfile(module_path + '/python/' + line.split[2] + '.py'):
                print('"' + line + '" cannot be loaded as route: it does not reference to an existing python file.')
                continue
            with open(module_path + '/python/' + linesplit[2] + '.py') as file_to_check:
                if not '\ndef ' + line.split(' ')[4] + '(' in file_to_check.read():
                    print('"' + line + '" cannot be loaded as route: the function specified is not within the referenced file.')
                    continue
            module_routes.append((line.split(' ')[0], line.split(' ')[2], line.split(' ')[4]))

    # The easiest way to see if a table declaration can be a table file, is to try and convert it's pieces to see if they make sense.'
    # This however, is more than the other two files have had at this point in terms of processessing since their main purpose
    # is unrealized so far. This is intentional.
    module_tables = []
    table_naming_whitelist = set(string.ascii_lowercase + '_')
    with open(module_path + '/structure/tables', 'r') as tables_file:
        tables = tables_file.read().split('[')
        for table in tables:
            lines = table.split('\n')
            table_dict = {
                    'table': '',
                    'columns':{},
                    'keys': []
                }
            # Table name verification
            if not ']' in lines[0]:
                print('Cannot load unknown table: No table name can be determined.')
                continue
            if not set(lines[0].split(']')[0]) <= table_naming_whitelist:
                print('Table name "' + lines[0].split(']')[0] + '" contains letters that are not lowercase or _.')
                continue
            table_dict['table'] = lines[0].split(']')[0]
            lines.pop(0)

            # Converting each row of the config into its MySQL version for use later.
            # This will fail if the file is not formatted right, meaning errors should be handled
            # by dropping the problem values and informing the user of an improper structure file.
            for line in lines:
                params = line.split(' ')

                # Handle keys first, since they undergo much less conversion.
                if params[0].upper() == params[0] and params[1] == '<':
                    if params[0] == 'NULL':
                        table_dict['columns'][params[2]] = table_dict['columns'][params[2]] + ' NULL'
                    if params[0] == 'PRIMARY':
                        table_dict['keys'].append('PRIMARY KEY (' + params[2] + ')')
                    if params[0] == 'UNIQUE':
                        table_dict['keys'].append('UNIQUE (' + params[2] + ')')

                # If we aint got keys, hopefully we got a column.
                if params[0].lower() == params[0] and params[1] == '>':
                    if not set(params[0]) <= table_naming_whitelist:
                        print('Column name "' + params[0] + '" contains letters that are not lowercase or _.')
                        continue
                    # !! After reviewing this secion, it requires the handling of attributes such as making integers
                    # unsigned. This is included in the format but is thrown away by this step. !!
                    column_type_size = params[2].split('-')[0]
                    column_size = 0
                    # Numbers can start anywhere in the string with the different options, so it's
                    # best to try and rebuild the size components using digit placement.
                    for character in column_type_size:
                        if character in string.digits:
                            column_size = column_size * 10
                            column_size = column_size + int(character)
                    column_type = column_type_size.replace(str(column_size), '')

                    sql = column_type.upper()

                    # Size Converter here is meant to take the numbers presented by
                    # the previous size step and turn them into their SQL counterpart.
                    # It should be formatted like the following:
                    # There should be a dict for a general type, one that shares a word
                    # with other parts of varying sizes.
                    # That should contain a -1, which due to previous steps cannot be used
                    # in table structure files. -1 defines how it is treated.
                    # Prefixs change the word, and suffixs are put in ( ) after the word.
                    # No size being specified by the file means that 0 will be used by
                    # default.
                    size_converter = {
                            'INT': {
                                -1: 'prefix',
                                0: '',
                                1: 'TINY',
                                2: 'SMALL',
                                3: 'MEDIUM',
                                4: '',
                                8: 'BIG'
                            },
                            'CHAR': {
                                -1: 'suffix',
                                0: '64'
                            },
                            'VARCHAR': {
                                -1: 'suffix',
                                0: '64'
                            },
                            'TEXT': {
                                -1: 'prefix',
                                0: '',
                                1: 'TINY',
                                2: '',
                                3: 'MEDIUM',
                                4: 'LONG'
                            }

                        }

                    # If I or anyone else has not used a datatype yet, it is likely not in here even if it should be
                    # I apologize, and would like an issue opened at the very least to know it is causing problems
                    # for at least one user.
                    if not sql in size_converter.keys():
                        print('Reality Rook module system currently does not support the ' + sql + ' datatype. If this is a datatype defined by MySQL, create or support an issue for it on the Rook Core Gitlab page.')
                        continue

                    append_method = size_converter[sql][-1]
                    if append_method == 'prefix':
                        sql = size_converter[sql][column_size] + sql
                    if append_method == 'suffix':
                        sql = sql + '(' + str(column_size) + ')'
                    table_dict['columns'][params[0]] = sql

            # This is low down, but it's the end of the table processing loop.
            module_tables.append(table_dict)

    # This is also low down, it's the end of the module processing loop.
    module_name = module_path.split('/')[-1]
    modules[module_name] = {
            'path': module_path,
            'tables': module_tables,
            'requires': module_require,
            'routes': module_routes
        }


# At this point any differences between the structure file defined tables
# and the tables present in the database need to be rectified. Checks against
# the MySQL syntax that represents current tables should be checked against
# the interpreted ones from the structure file. Adding new data should be automatic,
# like new tables and columns, but removing data needs to be passed by the user
# to make sure there are not irreversable mistakes.
database = mysql.connector.connect(
        host = config['host'],
        user = config['user'],
        password = config['password'],
        database = config['database']
    )

database_cursor = database.cursor(prepared=True)
check_sql = 'SHOW TABLES;'
database_cursor.execute(check_sql)
existing_table_arrays = database_cursor.fetchall()
existing_tables = []
for table_array in existing_table_arrays:
    existing_tables.append(table_array[0].decode())


for module_key in modules.keys():
    module = modules[module_key]

    for table in module['tables']:
        # This is for column checks and parsing.
        if table['table'] in existing_tables:
            column_check_sql = 'SHOW COLUMNS FROM ' +  table['table'] + ';'
            database_cursor = database.cursor(prepared=True)
            database_cursor.execute(column_check_sql)
            existing_column_arrays = database_cursor.fetchall()

            # these will be filled with data in the same format
            # as the structure file dictionaries.
            reconstructed_table = {}
            existing_keys = []
            existing_columns = {}
            for existing_column in existing_column_arrays:
                # param
                # 0 <- is the column name
                # 1 <- is the full datatype
                # 2 <- is the null treatment
                # 3 <- is key information

                # Name
                column_name = existing_column[0].decode()

                # Datatype
                reconstructed_sql = existing_column[1].decode().upper()

                # Null
                if existing_column[2].decode().upper() != 'NO':
                    reconstructed_sql = reconstructed_sql + ' NULL'

                # Assigning back for dict comparisons
                existing_columns[column_name] = reconstructed_sql

                # Key
                key_field = existing_column[3].decode()
                if key_field == 'PRI':
                    existing_keys.append('PRIMARY KEY (' + column_name + ')')
                if key_field == 'UNI':
                    existing_keys.append('UNIQUE (' + column_name + ')')

            reconstructed_table = {
                    'table': table['table'],
                    'columns': existing_columns,
                    'keys': existing_keys
                }
            if reconstructed_table == table:
                continue

            # Work with a damaged table. Damaged being a mismatch between configured structure and the table present in the database.
            # Action that can directly alter data in the database should be knowlingly intended by the user.
            print('Module ' + module_key + ' references a table in the database which has a different structure than the one specified.')
            print('This could be caused by an update to a module by a module developer, or a module conflict.')
            confirmation_response = input('Attempt to update ' + reconstructed_table['table'] + ' to match structure file? (Can destroy data, so be careful) [y/N]: ')
            if confirmation_response.lower() != 'y':
                print('Attempting to run with old table layout. If this causes issues, report them to the module developer.')
                continue

            if reconstructed_table['columns'] != table['columns']:
                for column_key in table['columns'].keys():
                    # Alter column if to the structuer file if it exists.
                    if column_key in reconstructed_table.keys() and table['columns'][column_key] != reconstructed_table['columns'][column_key]:
                        structure_file_sql = table['columns'][column_key]
                        reconstructed_table_sql = table['columns'][column_key]
                        sf_type = structure_file_sql.split(' ')[0]
                        recon_type = reconstructed_table_sql.split(' ')[0]
                        sf_null = structure_file_sql.split(' ')[-1]
                        recon_null = reconstructed_table_sql.split(' ')[-1]
                        if sf_type != recon_type:
                            database_cursor = database.cursor(prepared=True)
                            database_cursor.execute('ALTER TABLE ' + table['table'] + ' MODIFY ' + column_key + ' ' + sf_type + ';')
                            database.commit()
                            # Redfine with fix incase more is wrong.
                            reconstructed_table_sql.replace(recon_type, sf_type)
                            recon_type = reconstructed_table_sql.split(' ')[0]
                            recon_null = reconstructed_table_sql.split(' ')[-1]
                        if sf_null != recon_null:
                            redo_null_sql = 'ALTER TABLE ' + table['table'] + ' MODIFY ' + column_key + ' ' + sf_type
                            if sf_null == 'NULL':
                                redo_null_sql = redo_null_sql + ' NULL'
                            redo_null_sql = redo_null_sql + ';'
                            database_cursor = database.cursor(prepared=True)
                            database_cursor.execute(redo_null_sql)
                            database.commit()

                    # No colum, quick add.
                    if not (column_key in reconstructed_table.keys()):
                        database_cursor = database.cursor(prepared=True)
                        database_cursor.execute('ALTER TABLE ' + table['table'] + ' ADD ' + column_key + ' ' + table['columns'][column_key] + ';')
                        database.commit()

                # extra column, drop.
                for column_key in reconstructed_table['columns'].keys():
                    if not (column_key in table['columns'].keys()):
                        database_cursor = database.cursor(prepared=True)
                        database_cursor.execute('ALTER TABLE ' + table['table'] + ' ADD ' + column_key + ';')
                        database.commit()

            if reconstructed_table['keys'] != table['keys']:
                pass
                # key updates
                # if in table update,
                # if in recon delete

        # This is for making new tables.
        else:
            table_sql = 'CREATE TABLE ' + table['table']
            table_sql = table_sql + ' ('
            # Columns were formatted above to quick slot here.
            for column_key in table['columns'].keys():
                table_sql = table_sql + column_key + ' '
                table_sql = table_sql + table['columns'][column_key]
                # This may cause issues with other structuring above as it develops, but
                # for now NULL is preferred at the end.
                if table['columns'][column_key].split(' ')[-1] != 'NULL':
                    table_sql = table_sql + ' NOT NULL'
                table_sql = table_sql + ', '
            for key in table['keys']:
                table_sql = table_sql + key + ', '
            table_sql = table_sql[:-2] + ');'
            database_cursor = database.cursor(prepared=True)
            database_cursor.execute(table_sql)
            database.commit()


run(host='localhost', port=8080, debug=True)