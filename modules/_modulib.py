import json
import mysql.connector

if __name__ == '__main__':
    sys.exit("Modules cannot be ran directly.")

config = {}
with open('config/database.conf', 'r') as conf_file:
    config = json.loads(conf_file.read())

def check_table(structure_dict):
    database = db_prepare()
    sql = '''
    SELECT count(*)
    FROM information_schema.tables
    WHERE table_schema = %s
    AND table_name = %s;
    '''
    print(structure_dict['table'])
    database['cursor'].execute(sql, (config['database'], structure_dict['table'],))
    table_count = database['cursor'].fetchone()[0]
    if table_count < 1:
        sql = 'CREATE TABLE ' + structure_dict['table'] + ' ('
        for column in structure_dict['columns'].keys():
            sql = sql + column + ' ' + structure_dict['columns'][column]
            if column in structure_dict['keys']['unique']:
                sql = sql +' UNIQUE'
            sql = sql + ', '
        sql = sql + 'PRIMARY KEY (' + structure_dict['keys']['primary'] + '));'
        database['cursor'].execute(sql)
        database['connection'].commit()
        return

def db_prepare():
    database = mysql.connector.connect(
            host = config['host'],
            user = config['user'],
            password = config['password'],
            database = config['database']
        )

    database_cursor = database.cursor(prepared=True)
    return {
        'cursor': database_cursor,
        'connection': database
    }

def verify_request(auth_token):
    database = db_prepare()
    sql = 'SELECT id FROM auth_key WHERE token = %s;'
    result = database['cursor'].execute(sql, (auth_token))
    if (len(result.fetchall()) == 1):
        return True
    else:
        abort(401, 'Authorization Required')
        return False