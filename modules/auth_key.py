import _modulib as module
import sys
sys.path.insert(1, '../')

def _get_structure():
    return {
        'table': 'auth_key',
        'columns': {
            'id': 'INT',
            'token': 'VARCHAR(64) NOT NULL',
            'address': 'INT UNSIGNED'
            },
        'keys': {
            'primary': 'id',
            'unique': ['token']
            }
        }